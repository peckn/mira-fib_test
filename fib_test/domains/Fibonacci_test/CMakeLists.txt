
MIRA_REQUIRE_PACKAGE(Fibonacci_test)

###############################################################################

MIRA_ADD_DOCUMENTATION(Fibonacci_test
	DIRS
		doc
		include
	DEPENDS
		#RobotDataTypes
)

###############################################################################

INCLUDE_DIRECTORIES(${CMAKE_CURRENT_LIST_DIR}/include)

###############################################################################

MIRA_ADD_LIBRARY(FibonacciProducer
	SHARED
	PACKAGE Fibonacci_test
	SOURCE
		src/FibonacciProducer.C

	LINK_LIBS
		MIRABase
		MIRAFramework
)
